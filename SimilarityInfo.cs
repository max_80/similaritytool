using System.Collections.Generic;

namespace SimilarityTool
{
    public class SimilarityInfo
    {
        public string FileName { get; set; }
        public string Text { get; set; }
        public IList<int> Similarity { get; set; }
    }
}
