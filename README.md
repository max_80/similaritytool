# Similarity Tool
A tool designed to compare textfiles and produce a similarity matrix as a result.

## Prerequisites
An installation of [.NET Core v2](https://www.microsoft.com/net/download/windows) is required to run this project.

## Usage
```
$ git clone https://max_80@bitbucket.org/max_80/similaritytool.git
```
In SimilarityTool folder open power shell or console window and execute the follwing commands:
```
dotnet build
dotnet run \path\to\files
```
The **result.csv** file is placed inside the specified folder.

## Notes
The original Google Diff Match and Patch library has undergone slight changes to work with .NET Core.

## Copyright and License
The original [Google Diff Match and Patch](https://code.google.com/archive/p/google-diff-match-patch/) software is licensed under the [Apache License Version 2.0](http://www.apache.org/licenses/LICENSE-2.0)
http://code.google.com/p/google-diff-match-patch/ **Neil Fraser**

The Similarity Tool is licensed under the [MIT License](https://opensource.org/licenses/MIT)