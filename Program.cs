﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DiffMatchPatch;

namespace SimilarityTool
{
    class Program
    {
        private static int GetSimilarity(string text1, string text2)
        {
            var dmp = new diff_match_patch();
            var diffs = dmp.diff_main(text1, text2);

            var max = Math.Max(text1.Length, text2.Length);
            var distance = dmp.diff_levenshtein(diffs);

            var similarity = 100 - (distance * 100 / max);
            return similarity < 0 ? 0 : similarity;
        }
        static void Main(string[] args)
        {
            if(!args.Any())
            {
                Console.WriteLine("Please specify the source folder.");
                return;
            }
            var directory = args[0];
            if(!Directory.Exists(directory))
            {
                Console.WriteLine("Please specify correct source folder.");
                return;
            }

            var stopwatch = new Stopwatch();
            stopwatch.Start();
            
            var list = Directory.GetFiles(directory, "*.*").Select(file => 
                new SimilarityInfo
                {
                    FileName = new FileInfo(file).Name,
                    Text = File.ReadAllText(file)
                }
            ).ToList();
            
            Parallel.For(0, list.Count, ctr =>
            {
                var item = list[ctr];

                item.Similarity = (
                    from x in list
                        let text1 = item.Text
                        let text2 = x.Text
                        select item.FileName == x.FileName ? 100 : GetSimilarity(text1, text2)
                    ).ToList();

                Console.WriteLine(item.FileName);
            });

            var csvOutput = new StringBuilder();
            csvOutput.AppendLine($",{string.Join(",", list.Select(x => x.FileName))}");

            foreach (var item in list)
            {
                csvOutput.AppendLine($"{item.FileName},{string.Join(",", item.Similarity)}");
            }
            
            File.WriteAllText(Path.Combine(directory, "result.csv"), csvOutput.ToString());

            stopwatch.Stop();

            Console.WriteLine();
            Console.WriteLine($"Total: {list.Count} files");
            Console.WriteLine("Time elapsed: {0:hh\\:mm\\:ss}", stopwatch.Elapsed);
        }
    }
}
